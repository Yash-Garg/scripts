#!/usr/bin/env bash
#
# Copyright (C) Yash-Garg <ben10.yashgarg@gmail.com>
# SPDX-License-Identifier: GPL-v3.0-only
#

alias nfetch='neofetch | lolcat'
alias zs='source ~/.zshrc'
alias gitf='git commit --all -s -S'
